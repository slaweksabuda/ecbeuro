from rest_framework import routers
from currencies.api import RateViewSet

router = routers.DefaultRouter()

router.register(r'rates', RateViewSet)
