# ecbeuro
## About:
Scraper RSS, which reads the exchange rate from the European Central Bank and writes it to the database and the endpoint, which makes the data available via the REST API.  
[RSS Feeds list](https://www.ecb.europa.eu/home/html/rss.en.html).
## Used package
* Django==2.1.7
* djangorestframework==3.9.2
* requests==2.21.0

## Install
1. Clone or download repo  
2. Install requirements.txt, on your virtualenv run `pip install -r requirements.txt`
3. Migrations on database `./manage migrate` or `python manage.py migrate`

## Run scraper rss, download currencies rates and save to DB 
For run scraper rss use `./manage.py scraper_rss` or `python manage.py scraper_rss`

## Django rest framework
url `/api/v1/`
