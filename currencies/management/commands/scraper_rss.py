from django.core.management.base import BaseCommand
from currencies.utils import rewrite_rss_list, rewrite_currency_rate

class Command(BaseCommand):
    help = 'Srcaper RSS reading the exchange rate from the European Central Bank and recording it in the database '

    def handle(self, *args, **kwargs):
        rss = rewrite_rss_list()
        rates = rewrite_currency_rate()
        self.stdout.write(" %s" % rss)
        self.stdout.write(" %s" % rates)
