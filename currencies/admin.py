from django.contrib import admin
from .models import Currency, Rate

@admin.register(Currency)
class CurrencyAdmin(admin.ModelAdmin):
    list_display = [f.name for f in Currency._meta.fields]

@admin.register(Rate)
class RateAdmin(admin.ModelAdmin):
    raw_id_fields = ['currency']
    list_display = [f.name for f in Rate._meta.fields]
