from django.db import models
from django.utils.translation import ugettext_lazy as _


class TimeStampedModel(models.Model):
    """
    An abstract base class model that provides self-updating
    ``created`` and ``modified`` fields
    """
    created = models.DateTimeField(_('created at'), auto_now_add=True, db_index=True)
    modified = models.DateTimeField(_('modified at'), auto_now=True, db_index=True)

    class Meta:
        abstract = True


class Currency(TimeStampedModel):
    code = models.CharField(_('code'), max_length=3, unique=True)
    name = models.CharField(_('name'), max_length=125)
    url = models.URLField(_('url'))

    class Meta:
        verbose_name = _('currency')
        verbose_name_plural = _('currencies')
        ordering = ['code']

    def __str__(self):
        return self.code


class Rate(TimeStampedModel):
    currency = models.ForeignKey(Currency, on_delete=models.CASCADE)
    date = models.DateField(_('date'))
    rate = models.DecimalField(_('rate'), max_digits=12, decimal_places=4)

    class Meta:
        verbose_name = _('Currency rate')
        verbose_name_plural = _('Currency rates')
        ordering = ['-date', 'currency']

    def __str__(self):
        return "{c} - {d} - {r} ".format(c=self.currency, d=self.date, r=self.rate)
