from rest_framework import viewsets, permissions

from .serializers import RateSerializer
from .models import Rate


class RateViewSet(viewsets.ModelViewSet):
    queryset = Rate.objects.all()
    serializer_class = RateSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, )
