import xml.etree.ElementTree as ET
from re import findall, match

import requests
from django.conf import settings
from django.db import IntegrityError

from .models import Currency, Rate


def rewrite_rss_list():
    """
    Function for rewrite currencies and they url from https://www.ecb.europa.eu/home/html/rss.en.html
    :return: if error list return error_list and currency else return only currency_list
    """
    error_list = []
    currency_list = []
    url = getattr(settings, 'ECB_RSS_LIST_URL', '')
    r = requests.get(url)
    if r.status_code != 200:
        return r
    rss = findall('(.*?)(<li><a class="rss" href=")(/rss/fxref-)([a-z]{3})(\.html)(">)([\w+\s]{1,})', r.text)
    # example tuple
    # ('\t', '<li><a class="rss" href="', '/rss/fxref-', 'usd', '.html', '">', 'US dollar ')

    for cur in rss:
        try:
            currency_dict = {'code': cur[3],
                             'name': cur[6],
                             'url': '{}{}{}{}'.format(getattr(settings, 'ECB_BASE_URL'), cur[2], cur[3], cur[4])
                             }
            if Currency.objects.filter(code=currency_dict.get('code')).exists():
                # if exists update
                currency = Currency.objects.get(code=currency_dict.get('code'))
                for key, value in currency_dict.items():
                    setattr(currency, key, value)
                    currency.save()
            else:
                # else create new
                currency = Currency(**currency_dict)
                currency.save()
        except (KeyError, IntegrityError) as e:
            error_list.append(str(e))
            continue
        else:
            currency_list.append(currency)

    if error_list:
        return {'errors': error_list,
                'currency': currency_list
                }

    return currency_list


def rewrite_currency_rate():
    """
    Function for rewrite currency rates from xml to DB
    :return: if error list return error_list and rates else return only rates
    """

    error_list = []
    rates_list = []

    for cur in Currency.objects.all():
        try:
            r = requests.get(cur.url)
            tree = ET.ElementTree(ET.fromstring(r.text))
            root = tree.getroot()
            for item in root:
                m = match('(.*?)(\?date=)(\d{4}-\d{2}-\d{2})(\&rate=)(\d{1,8}\.\d{1,4})',
                          item.attrib.get('{http://www.w3.org/1999/02/22-rdf-syntax-ns#}about'))
                if m:
                    rate = Rate.objects.get_or_create(currency=cur, date=m.group(3), rate=m.group(5))[0]
        except Exception as e:
            error_list.append(str(e))
            continue
        else:
            rates_list.append(rate)

    if error_list:
        return {'errors': error_list,
                'rates_list': rates_list
                }
    return rates_list
