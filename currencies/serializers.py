from rest_framework import serializers
from .models import Rate


class RateSerializer(serializers.ModelSerializer):
    currency_name = serializers.ReadOnlyField(source='currency.name')
    currency_code = serializers.ReadOnlyField(source='currency.code')

    class Meta:
        model = Rate
        fields = ('id', 'created', 'modified', 'currency', 'currency_code', 'currency_name', 'date', 'rate')
